// Century Time
// The time zero is 2000-01-01 00:00:00 UTC
// Time is represented with two int64 numbers.
// One for Nanoseconds and one for Century.
// Time offset is only applied to Century
// Time offset is applied to Century.
// Time offset at less then century is calc with Go time.Time
//
// Time Human representation is:
//	Georgian Time = 1999-12-31
//	Century Time  = 19-99-12-31

// This code is pre pre Alpha
package century

import (
	"fmt"
	"time"

	"bitbucket.org/gotamer/conv"
)

const (
	unixAt2000   int64 = 946684800000000000 // NanoSecondes
	julianAt2000 int64 = 2451545            // Days
	goZero       int64 = -292277022399      // From UNIX

	nanoSecondsPerSecond = 1000 * 1000 * 1000
	secondsPerDay        = 24 * 60 * 60
	daysPerCentury       = 36524.25

	nanoSecondsPerCentury = daysPerCentury * secondsPerDay * nanoSecondsPerSecond
	nanoSecondsPerYear    = nanoSecondsPerCentury / 100
	nanoSecondsPerMonth   = nanoSecondsPerYear / 12
)

// Century Zero is:
var centuryZero = time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)

// Century Time Format
type Time struct {
	Century int64
	NanoSec int64
}

// Century Time Now
func Now() (ct *Time) {
	ct = new(Time)
	ct.NanoSec = time.Now().UnixNano()
	ct.At()
	return
}

// Takes UNIX NanaSeconds
func (ct *Time) At() {
	var unixns, uns int64
	var negative bool

	unixns = ct.NanoSec

	if unixns < 0 {
		unixns = reverseSign(unixns)
		negative = true
		fmt.Println("Reversed sign")
	}

	if unixns > unixAt2000 {
		uns = unixns - unixAt2000
		ct.calc(uns)
	} else {
		uns = unixAt2000 - unixns
		ct.calc(uns)
		ct.reversSign()
	}

	if negative {
		ct.reversSign()
	}
}

func (ct *Time) calc(uns int64) {
	if uns > nanoSecondsPerCentury {
		ct.Century = uns / nanoSecondsPerCentury
		uns = uns - ct.Century*nanoSecondsPerCentury
	}
	ct.NanoSec = uns
}

func (ct *Time) reversSign() {
	ct.Century = reverseSign(ct.Century)
	ct.NanoSec = reverseSign(ct.NanoSec)
	fmt.Println("Reversed sign back")
}

const centuryTimeBinary = 1

// MarshalBinary implements the encoding.BinaryMarshaler interface.
func (ct *Time) MarshalBinary() (data []byte, err error) {
	return []byte{
		byte(centuryTimeBinary), // encoding version
		byte(ct.Century >> 56),  // bytes 1-8: Hours
		byte(ct.Century >> 48),
		byte(ct.Century >> 40),
		byte(ct.Century >> 32),
		byte(ct.Century >> 24),
		byte(ct.Century >> 16),
		byte(ct.Century >> 8),
		byte(ct.Century),
		byte(ct.NanoSec >> 56), //bytes 9-16: nanoseconds
		byte(ct.NanoSec >> 48),
		byte(ct.NanoSec >> 40),
		byte(ct.NanoSec >> 32),
		byte(ct.NanoSec >> 24),
		byte(ct.NanoSec >> 16),
		byte(ct.NanoSec >> 8),
		byte(ct.NanoSec),
	}, nil
}

// UnmarshalBinary implements the encoding.BinaryUnmarshaler interface.
func (ct *Time) UnmarshalBinary(data []byte) error {
	if data[0] != centuryTimeBinary {
		return fmt.Errorf("Wrong data type")
	}
	if len(data) != 17 {
		return fmt.Errorf("Wrong data type, data len() don't match")
	}
	ct.Century = conv.Btoi(data[1:9])
	ct.NanoSec = conv.Btoi(data[9:])
	return nil
}

// Float Time Now
func FloatNow() float64 {
	return time.Since(centuryZero).Seconds()
}

// Float Time at the given Go time.Time
func FloatAt(t time.Time) float64 {
	return t.Sub(centuryZero).Seconds()
}

// Float Time to Go time.Time
func FloatGo(centuryTime float64) time.Time {
	return centuryZero.Add(time.Duration(centuryTime * 1e9))
}

func reverseSign(i int64) int64 {
	return i - i*2
}
