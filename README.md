Century Time
============
The time zero is 2000-01-01 00:00:00 UTC  
Time is represented with two int64 numbers.  
One for Nanoseconds and one for Century.  
Time offset is applied to Century.

Time offset at less then century is calc with Go time.Time

Time representation is:

	Georgian Time = 1999-12-31 
	Century Time  = 19-99-12-31

#### This code is pre pre Alpha

### Install
	go get bitbucket.org/gotamer/century


### The Code Is


	// Century Time Now
	func Now() century.Time
	
	// Century Time at the given time.UnixNano()
	func At(t time.Time) century.Time
	
	// Century Time to time.UnixNano()
	func Go(ct century.Time) time.Time



	// Float Time Now
	func FloatNow() float64
	
	// Float Time at the given Go time.Time
	func FloatAt(t time.Time) float64
	
	// Float Time to Go time.Time
	func FloatGo(centuryTime float64) time.Time
